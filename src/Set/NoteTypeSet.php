<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CAPWELTON ({@link http://www.capwelton.com})
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */
namespace Capwelton\App\Note\Set;

/**
 * A NoteType is a type that can be associated to any note.
 *
 * @property \ORM_StringField   $name
 * @property \ORM_BoolField     $isDefault
 * 
 * @method  \Func_App                   App()
 * @method  NoteType                    newRecord()
 * @method  NoteType                    get()
 * @method  NoteType                    request()
 * @method  NoteType[]|\ORM_Iterator    select(\ORM_Criteria $criteria)
 */
class NoteTypeSet extends \app_TraceableRecordSet
{

	public function __construct(\Func_App $App = null)
	{
		parent::__construct($App);
		
		$this->setTableName($App->classPrefix.'NoteType');

		$this->setDescription('Note type');

		$this->setPrimaryKey('id');
		
		$appC = $App->getComponentByName('NoteType');

		$this->addFields(
		    ORM_StringField('name')->setDescription($appC->translate('Name')),
		    ORM_BoolField('isDefault')->setOutputOptions($appC->translate('No'), $appC->translate('Yes'))
		);
	}
	
	/**
	 *
	 * {@inheritdoc}
	 * @see \app_TraceableRecordSet::save()
	 */
	public function save(\ORM_Record $record, $noTrace = false)
	{
	    $event = new NoteTypeBeforeSaveEvent($record);
	    bab_fireEvent($event);
	    
	    $result = parent::save($record);
	    
	    $event = new NoteTypeAfterSaveEvent($record);
	    bab_fireEvent($event);
	    
	    return $result;
	}
	
	public function onUpdate()
	{
	    $App = $this->app();
	    $component = $App->getComponentByName('Note');
	    $noteTypeSet = $App->NoteTypeSet();
	    $created = 0;
	    if($noteTypeSet->select()->count() == 0){
	        $defaultNoteType = $noteTypeSet->newRecord();
	        $defaultNoteType->name = $component->translate('General');
	        $defaultNoteType->isDefault = true;
	        $defaultNoteType->save();
	        $defaultNoteType->setDefaultBlueprint();
	        $created++;
	    }
	    if($created > 0){
	        $message = $component->translate('Default note types initialized');
	        $color = 'green';
	        $message = "<span style='color:{$color};'>{$message}</span>";
	        \bab_installWindow::message($message);
	    }
	}
	
	/**
	 * {@inheritDoc}
	 * @see \app_Record::isReadable()
	 */
	public function isReadable()
	{
	    return $this->all();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \app_RecordSet::isCreatable()
	 */
	public function isCreatable()
	{
	    return true;
	}

	public function isUpdatable()
	{
	    return $this->all();
	}
	
	public function isDeletable()
	{
	    return $this->all();
	}
	
	public function getDefaultBlueprint()
	{
	    $set = $this->App()->NoteTypeSet();
	    $default = $set->get($set->isDefault->is(true));
	    if($default){
	        return $default->getBlueprint();
	    }
	    return $set->newRecord()->getClassName().':0';
	}
}

class NoteTypeBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class NoteTypeAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}