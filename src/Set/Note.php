<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CAPWELTON ({@link http://www.capwelton.com})
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */
namespace Capwelton\App\Note\Set;


use Capwelton\App\Note\Ctrl\NoteController;

/**
 * A Note is a text note that can be associated to any record.
 *
 * @property string         $summary
 * @property bool           $private
 * @property bool           $pinned
 * @property string         $description
 * @property NoteType       $type
 * 
 * @method  \Func_App       App()
 * @method  NoteType        type()
 */
class Note extends \app_TraceableRecord
{
	const SUBFOLDER = 'notes';

	/**
	 * {@inheritDoc}
	 * @see \app_Record::isUpdatable()
	 */
	public function isUpdatable()
	{
	    return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \app_Record::getController()
	 * @return NoteController
	 */
	public function getController()
	{
	    $App = $this->App();
	    return $App->Controller()->Note();
	}

	/**
	 * Get the upload path for files related to this note.
	 * @return \bab_Path
	 */
	public function uploadPath()
	{
		if (!isset($this->id)) {
			return null;
		}

		require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

		$path = $this->App()->getUploadPath();
		$path->push(self::SUBFOLDER);
		$path->push($this->id);
		return $path;
	}


	/**
	 * {@inheritDoc}
	 * @see \app_Record::linkTo()
	 */
	public function linkTo(\app_Record $source, $linkType = 'hasNote')
	{
		return parent::linkTo($source, $linkType);
	}



	/**
	 * Attach a file to this note.
	 * File is provided by a filepicker widget
	 *
	 * @see \Widget_FilePickerIterator
	 *
	 * @param	\Widget_FilePickerItem	$file
	 * @return self
	 */
	public function attachFile(\Widget_FilePickerItem $file)
	{
		$uploadPath = $this->uploadPath();
		$uploadPath->createDir();
		$original = $file->getFilePath()->toString();
		$uploadPath->push(basename($original));

		rename($original, $uploadPath->toString());
		
		return $this;
	}

	/**
	 * Get iterator with attachements
	 * @return \Widget_FilePickerIterator
	 */
	public function attachments()
	{
		return bab_Widgets()->FilePicker()->getFolderFiles($this->uploadPath());
	}

	/**
	 * @return Note[]\ORM_Iterator
	 */
	public function getLinkedNotes($linkType = 'hasNote')
	{
	    $App = $this->App();

	    $linkSet = $App->LinkSet();

	    $links = $linkSet->selectForTarget($this, $this->getClassName(), $linkType);

	    $notes = array();
	    foreach ($links as $link) {
	        $notes[] = $link->sourceId;
	    }

	    return $notes;
	}


	/**
	 * {@inheritDoc}
	 * @see \ORM_Record::getRecordTitle()
	 */
	public function getRecordTitle()
	{
        return bab_abbr($this->summary, BAB_ABBR_FULL_WORDS, 60);
	}
	
	/**
	 * Returns the view name associated to the type of this note
	 * @return string The view name
	 */
	public function getBlueprint()
	{
	    if($type = $this->type()){
	        return $type->getBlueprint();
	    }
	    
	    /* @var $typeSet NoteTypeSet */
	    $typeSet = $this->App()->NoteTypeSet();
	    $dummy = $typeSet->newRecord();
	    $dummy->id = 0;
	    
	    return $dummy->getBlueprint();
	}
}
