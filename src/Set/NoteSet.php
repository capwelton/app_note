<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CAPWELTON ({@link http://www.capwelton.com})
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */
namespace Capwelton\App\Note\Set;

/**
 * A Note is a text note that can be associated to any record.
 *
 * @property \ORM_TextField         $summary
 * @property \ORM_BoolField         $private
 * @property \ORM_BoolField         $pinned
 * @property \ORM_LongTextField     $description
 * @property NoteTypeSet            $type
 * 
 * @method  NoteTypeSet             type()
 * @method  \Func_App               App()
 * @method  Note                    newRecord()
 * @method  Note                    get()
 * @method  Note                    request()
 * @method  Note[]|\ORM_Iterator    select(\ORM_Criteria $criteria)
 */
class NoteSet extends \app_TraceableRecordSet
{

	public function __construct(\Func_App $App = null)
	{
		parent::__construct($App);
		
		$this->setTableName($App->classPrefix.'Note');

		$this->setDescription('Note');

		$this->setPrimaryKey('id');
		
		$appC = $App->getComponentByName('Note');

		$this->addFields(
		    ORM_TextField('summary')->setDescription($appC->translate('Title')),
		    ORM_BoolField('private')->setDescription($appC->translate('Private')),
		    ORM_BoolField('pinned')->setDescription($appC->translate('Pinned')),
		    ORM_LongTextField('description')->setDescription($appC->translate('Description'))
		);
		
		$this->hasOne('type', $App->NoteTypeSetClassName());
	}
	
	/**
	 *
	 * {@inheritdoc}
	 * @see \app_TraceableRecordSet::save()
	 */
	public function save(\ORM_Record $record, $noTrace = false)
	{
	    $event = new NoteBeforeSaveEvent($record);
	    bab_fireEvent($event);
	    
	    $result = parent::save($record);
	    
	    $event = new NoteAfterSaveEvent($record);
	    bab_fireEvent($event);
	    
	    return $result;
	}
	
	public function getRequiredComponents()
	{
	    return array(
	        'NoteType'
	    );
	}
	
	public function getOptionalComponents()
	{
	    return array(
	        'Attachment'
	    );
	}
	/**
	 * {@inheritDoc}
	 * @see \app_Record::isReadable()
	 */
	public function isReadable()
	{
	    return $this->any(
	        $this->private->isNot(true),
	        $this->createdBy->is(bab_getUserId())
        );
	}
	
	/**
	 * {@inheritDoc}
	 * @see \app_RecordSet::isCreatable()
	 */
	public function isCreatable()
	{
	    return true;
	}

	public function isUpdatable()
	{
	    return $this->any($this->createdBy->is(bab_getUserId()));
	}
	
	public function isDeletable()
	{
	    return $this->any($this->createdBy->is(bab_getUserId()));
	}

	/**
	 * Returns an iterator of notes linked to the specified source,
	 * optionally filtered on the specified link type.
	 *
	 * @return \ORM_Iterator
	 */
	public function selectLinkedTo($source, $linkType = 'hasNote')
	{
		return parent::selectLinkedTo($source, $linkType);
	}
}

class NoteBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class NoteAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}