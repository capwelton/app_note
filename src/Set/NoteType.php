<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CAPWELTON ({@link http://www.capwelton.com})
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */
namespace Capwelton\App\Note\Set;

use Capwelton\App\Note\Ctrl\NoteController;


/**
 * A NoteType is a type that can be associated to any note.
 *
 * @property string		$name
 * @property bool       $isDefault
 * 
 * @method  \Func_App   App()
 */
class NoteType extends \app_TraceableRecord
{
    
    /**
     * {@inheritDoc}
     * @see \app_Record::getController()
     * @return NoteController
     */
    public function getController()
    {
        $App = $this->App();
        return $App->Controller()->Note();
    }
    
    /**
     * Returns the view name associated to this type
     * @return string The view name
     */
    public function getBlueprint()
    {
        return $this->getRef();
    }
    
    /**
     * Generate a general blueprint (view), containing sections with generic fields
     * @param bool $force If true, existing view will be deleted
     * @return self
     */
    public function setDefaultBlueprint($force = false)
    {
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();
        $object = $App->classPrefix.'Note';
        $view = $this->getBlueprint();
        
        $sectionCriteria = $customSectionSet->all(
            $customSectionSet->object->is($object)
            ->_AND_($customSectionSet->view->is($view))
        );
        
        $customSections = $customSectionSet->select(
            $sectionCriteria
        );
        
        if($customSections->count() > 0 && !$force)
        {
            return $this;
        }
        
        $customContainerSet = $App->CustomContainerSet();
        
        $containerCriteria = $customContainerSet->all(
            $customContainerSet->object->is($object)
            ->_AND_($customContainerSet->view->is($view))
        );
        
        $customContainerSet->delete($containerCriteria);
        $customSectionSet->delete($sectionCriteria);
        
        $rank = 0;
        
        $container = $customContainerSet->newRecord();
        $container->view = $view;
        $container->object = $object;
        $container->sizePolicy = 'col-md-12';
        $container->layout = 'vbox';
        $container->save();
        
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-12';
        $customSection->fieldsLayout = 'verticalLabel';
        $customSection->object = $object;
        $customSection->fields = '{"description":{"block":"","fieldname":"description","parameters":[]},"attachments":{"block":"","fieldname":"attachments","parameters":[]}}';
        $customSection->classname = 'box red';
        $customSection->editable = true;
        $customSection->name = '';
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->container = $container->id;
        $customSection->save();
        
        return $this;
    }
}
