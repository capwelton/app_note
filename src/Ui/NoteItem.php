<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Note\Ui;
use Capwelton\App\Note\Set\Note;
use Capwelton\App\Note\Set\NoteSet;

/**
 * @return NoteItem
 * @method self addItem(Widget_Displayable_Interface $item = null)
 */
class NoteItem extends \app_UiObject
{
    /**
     * @var Note
     */
    protected $note;
    
    /**
     * @var NoteSet
     */
    protected $set;
    
    protected $noteComponent = null;
    
    public function __construct(\Func_App $App, Note $note, \Widget_Layout $layout = null, $itemId = null)
    {
        parent::__construct($App);
        
        $this->note = $note;
        $this->set = $note->getParentSet();
        $this->noteComponent = $App->getComponentByName('Note');
        
        $W = bab_Widgets();
        
        $this->setInheritedItem($W->Frame($itemId, $layout));
        
        $this->computeContent();
    }
    
    public function computeContent()
    {
        $W = bab_Widgets();
        
        $actionsBox = $W->FlowItems();
        $actionsBox->addClass('widget-nowrap');
        
        $record = $this->note;
        $App = $this->App();
        
        $ctrl = $App->Controller()->Note();
        
        $menuItems = array();
        if ($record->isUpdatable()) {
            $menuItems[] = $W->Link(
                $this->noteComponent->translate('Edit'),
                $ctrl->edit($record->id)
            )->setOpenMode(\Widget_Link::OPEN_DIALOG)
            ->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT);
        }
        if ($record->isDeletable()) {
            $menuItems[] = $W->Link(
                $this->noteComponent->translate('Delete'),
                $ctrl->confirmDelete($record->id)
            )->setOpenMode(\Widget_Link::OPEN_DIALOG)
            ->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE);
        }
        
        if(!empty($menuItems)){
            $menu = $W->Menu(null, $W->VBoxItems());
            $menu->addClass(\Func_Icons::ICON_LEFT_16);
            foreach ($menuItems as $menuItem){
                $menu->addItem($menuItem);
            }
            $actionsBox->addItem($menu);
        }
        
        $attachments = $record->attachments();
        
        $nbFiles = 0;
        if ($attachments) {
            $attachmentsMenu = $W->Menu(null, $W->VBoxItems());
            $attachmentsMenu->setButtonLabel('');
            $attachmentsMenu->setButtonClass('icon ' . \Func_Icons::MIMETYPES_UNKNOWN);
            $attachmentsMenu->addClass(\Func_Icons::ICON_LEFT_16);
            $attachmentsMenu->setSizePolicy(\Func_Icons::ICON_LEFT_16);
            foreach ($attachments as $attachment) {
                $nbFiles++;
                $attachmentsMenu->addItem(
                    $W->Link(
                        $attachment->toString(),
                        $ctrl->downloadAttachment($record->id, $attachment->getFileName(), 0)
                    )->addClass('icon', \Func_Icons::MIMETYPES_UNKNOWN)
                );
            }
        }
        if ($nbFiles > 0) {
            $actionsBox->addItem($attachmentsMenu, 0);
        }
        
        $listItem = $W->VBoxItems();
        
        $chip = $record->type() ? $App->Ui()->Chip($record->type()->name) : null;
        
        $title = $record->summary;
        
        $text = strip_tags($title);
        $textLen = strlen($text);
        if($textLen > 0){
            if($textLen > 50){
                $text = substr($text, 0, 50).'...';
            }
        }
        
        $listItem->addItem(
            $W->HBoxItems(
                $W->HBoxItems(
                    $W->VBoxItems(
                        $W->Link(
                            $W->Title($text, 6),
                            $ctrl->display($record->id)
                        )->setOpenMode(\Widget_Link::OPEN_DIALOG_EXTENDED),
                        $W->Label(\BAB_DateTime::fromIsoDateTime($record->createdOn)->shortFormat())
                    ),
                    $W->FlowItems(
                        $chip
                    )->addClass('widget-align-right')
                    ->setHorizontalSpacing(2, 'px')
                    ->setSizePolicy('pull-right')
                )->setSizePolicy(\Widget_SizePolicy::MAXIMUM)
                ->setVerticalAlign('middle')
                ->setHorizontalSpacing(8, 'px')
                ->addClass(\Func_Icons::ICON_LEFT_16)
            )->addClass('widget-100pc')
            ->setVerticalAlign('middle')
            ->setHorizontalSpacing(8, 'px')
        );
        
        $this->addItem($listItem);
    }
}