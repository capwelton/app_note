<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Note\Ui;
use Capwelton\App\Note\Set\Note;
use Capwelton\App\Note\Set\NoteType;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

/**
 * @return NoteEditor
 */
class NoteEditor extends \app_RecordEditor
{
    private $filepicker = null;

    protected $note = null;
    protected $noteTypes = array();
    protected $reloaded = false;
    
    protected $noteTypeComponent = false;


    /**
     * @param \Func_App $app
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $app, \app_Record $note = null, $reloaded = false, $id = null, \Widget_Layout $layout = null)
    {
        $component = $app->getComponentByName('NoteType');
        if($component){
            $noteTypeSet = $app->NoteTypeSet();
            $this->noteTypes = $noteTypeSet->select();
            $this->noteTypeComponent = $component;
        }
        
        $this->note = $note;
        $this->reloaded = $reloaded;
        
        parent::__construct($app, $id, $layout);
        
        $this->setHiddenValue('tg', $app->controllerTg);
        $this->setSaveAction(
            $app->Controller()->Note()->save(),
            $this->noteTypeComponent->translate('Add this note')
        );
    }
    
    protected function buttonsLayout()
    {
        $W = bab_Widgets();
        $btnLayout = $W->HBoxLayout();
        
        if(!$this->reloaded){
            $btnLayout->setSizePolicy('widget-form-buttons')->setHorizontalSpacing(1, 'em');
        }
        
        return $btnLayout;
    }
    
    protected function appendButtons()
    {
        if(!$this->reloaded){
            $W = $this->widgets;
            $App = $this->App();
            
            if (isset($this->saveAction)) {
                $saveLabel = isset($this->saveLabel) ? $this->saveLabel : $this->noteTypeComponent->translate('Save');
                $submitButton = $W->SubmitButton();
                $submitButton->validate()
                ->setAction($this->saveAction)
                ->setFailedAction($this->failedAction)
                ->setSuccessAction($this->successAction)
                ->setLabel($saveLabel);
                if ($this->isAjax) {
                    $submitButton->setAjaxAction();
                }
                $this->addButton($submitButton);
            }
            
            if (isset($this->cancelAction)) {
                $cancelLabel = isset($this->cancelLabel) ? $this->cancelLabel : $this->noteTypeComponent->translate('Cancel');
                $this->addButton(
                    $W->SubmitButton(/*'cancel'*/)
                    ->addClass('widget-close-dialog')
                    ->setAction($this->cancelAction)
                    ->setLabel($cancelLabel)
                );
            }
        }
    }


    /**
     * Add fields into form
     * @return note.editor
     */
    public function prependFields($withOptions = true, $withAttachements = true)
    {
        $W = $this->widgets;
        $this->addItem($this->NoteType());
        
        $this->NoteSections();

        if ($withOptions) {
            $this->addItem(
                $W->FlowItems(
                    $this->PrivateItem(),
                    $this->PinnedItem()
                )
                ->setHorizontalSpacing(2, 'em')
                ->setVerticalSpacing(1, 'em')
            );
        }
        
        $this->addItem($this->getFilePicker());

        return $this;
    }

    protected function NoteType()
    {        
        $options = array(
            0 => ''
        );
        
        foreach($this->noteTypes as $noteType){
            $options[$noteType->id] = $noteType->name;
        }
        
        if(count($options) == 1){
            //No type, return nothing, as the type is not mandatory
            return null;
        }
        
        $W = $this->widgets;
        
        $select = $W->Select();
        $select->setOptions($options);
        
        return $this->labelledField(
            $this->noteTypeComponent->translate('Type'),
            $select,
            'type'
        );
    }
    
    protected function getBlueprint()
    {
        if($this->note && $type = $this->note->type()){
            return $type->getBlueprint();
        }
        
        return $this->App()->NoteTypeSet()->getDefaultBlueprint();
    }
    
    protected function NoteSections()
    {        
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();
        
        $customSections = $customSectionSet->select($customSectionSet->view->is($this->getBlueprint())->_AND_($customSectionSet->object->is('Note')));
        $customSections->orderAsc($customSectionSet->rank);
        
        $editor = new \app_RecordEditor($App);
        $editor->recordSet = $this->note->getParentSet();
        $editor->setRecord($this->note);
        
        foreach ($customSections as $customSection){
            $section = $editor->addSection($customSection->id, $customSection->name);
            $section->setFoldable($customSection->foldable, $customSection->folded);
            $section->addClass($customSection->classname);
            $section->setSizePolicy($customSection->sizePolicy);
            $this->addItem($section);
            
            $displayFields = $customSection->getFields();
            
            foreach ($displayFields as $displayField) {
                $widget = null;
                $item = null;
                $displayFieldName = $displayField['fieldname'];
                $parameters = $displayField['parameters'];
                $classname = isset($parameters['classname']) ? $parameters['classname'] : '';
                $label = isset($parameters['label']) && $parameters['label'] !== '__' ? $parameters['label'] : '';
                $displayFieldMethod = '_' . $displayFieldName;
                if (method_exists($this, $displayFieldMethod)) {
                    $widget = $this->$displayFieldMethod($customSection, $label);
                    $item = $widget;
                } elseif ($editor->recordSet->fieldExist($displayFieldName)) {
                    $field = $editor->recordSet->getField($displayFieldName);
                    if ($label === '') {
                        $label = $field->getDescription();
                        if (substr($displayFieldName, 0, 1) !== '_') {
                            $label = $this->noteTypeComponent->translate($label);
                        }
                    }
                    $widget = $field->getWidget();
                    if ($widget instanceof \Widget_TextEdit || $widget instanceof \Widget_Select) {
                        $widget->addClass('widget-100pc');
                    }
                    $item = $editor->getValueItem($customSection, $displayField, $label, $widget);
                }
                
                if ($item) {
                    $item->addClass($classname);
                    $section->addItem($item);
                }
            }
            
        }
        
    }

    public function _summary($customSection)
    {
        $App = $this->App();
        $W = $this->widgets;
        $summaryFormItem = $W->LineEdit()->addClass('widget-fullwidth');
        $summaryFormItem->setMandatory(true, $this->noteTypeComponent->translate('The note title must not be empty'));

        return $this->labelledField($this->noteTypeComponent->translate('Title'), $summaryFormItem);
    }


    protected function PrivateItem()
    {
        $W = $this->widgets;
        $App = $this->App();

        return $this->labelledField(
            $this->noteTypeComponent->translate('Private note'),
            $W->CheckBox(),
            'private',
            $this->noteTypeComponent->translate('Private notes are only visible by their author')
        );
    }


    protected function PinnedItem()
    {
        $W = $this->widgets;
        $App = $this->App();

        return $this->labelledField(
            $this->noteTypeComponent->translate('Pinned note'),
            $W->CheckBox(),
            'pinned',
            $this->noteTypeComponent->translate('Pinned notes stay on top of history')
        );
    }


    public function setNote(Note $note = null)
    {
        if (isset($note)) {
            $this->note = $note;
            $noteValues = $note->getValues();
            $this->setValues(array('note' => $noteValues));

            if (!empty($note->id)) {
                $this->setHiddenValue('data[id]', $note->id);
            }
        }


        if (isset($note)) {
            $App = $this->App();
            $this->setSaveAction($App->Controller()->Note()->save(), $this->noteTypeComponent->translate('Save this note'));
        } else {

        }
    }

    /**
     * set editor in create note mode
     */
    public function addNote()
    {
        $this->setNote();
    }



    /**
     * Get file picker widget
     * @see NoteEditor::prependFields()
     *
     * @return \Widget_FilePicker
     */
    public function getFilePicker()
    {
        if (null === $this->filepicker) {

            $W = $this->widgets;

            $this->filepicker = $W->FilePicker()
                ->setName('attachments')
            //	->hideFiles()
            ;

            if (isset($this->note))
            {
                $this->filepicker->setFolder($this->note->uploadPath());
            }
        }

        return $this->filepicker;
    }


}

