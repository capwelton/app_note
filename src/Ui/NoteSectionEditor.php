<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Note\Ui;

use Capwelton\App\Note\Set\Note;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

class NoteSectionEditor extends \app_RecordEditor
{
    protected $noteComponent = null;
    
    public function __construct(Note $note, \Func_App $app, $id = null, $layout = null)
    {
        $this->noteComponent = $app->getComponentByName('Note');
        parent::__construct($app, $note, $id, $layout);   
        $this->setRecord($note);
    }
    
    /**
     * @return \Widget_Layout
     */
    protected function buttonsLayout()
    {
        $W = bab_Widgets();
        
        $box = $W->HBoxItems();
        
        if(!isset($this->itemId)){
            $box->setSizePolicy('widget-form-buttons')
            ->setHorizontalSpacing(1, 'em');
        }
        
        return $box;
    }
    
    protected function _summary($customSection)
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->noteComponent->translate('Title'),
            $W->LineEdit(),
            'summary'
        );
    }
    
    protected function _description($customSection)
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->noteComponent->translate('Description'),
            $W->CKEditor(),
            'description'
        );
    }
    
    protected function _type($customSection)
    {
        $W = $this->widgets;
        $App = $this->App();
        
        $noteTypeSet = $App->NoteTypeSet();
        $noteTypes = $noteTypeSet->select();
        
        
        $select = $W->Select();
        $select->addOption(0, '');
        
        foreach ($noteTypes as $noteType) {
            $select->addOption($noteType->id, $noteType->name);
        }
        
        return $this->labelledField(
            $this->noteComponent->translate('Type'),
            $select,
            'type'
        );
    }
    
    protected function _attachments(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $attachmentComponent = $App->getComponentByName('ATTACHMENT');
        if(!isset($attachmentComponent)){
            return null;
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $attachmentComponent->translate('Attachments'),
            $App->Controller()->Attachment(false)->_attachments($this->record->getRef()),
            $customSection
        );
    }
}