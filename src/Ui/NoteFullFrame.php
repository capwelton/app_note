<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Note\Ui;
use Capwelton\App\Note\Set\Note;
use Capwelton\App\Note\Set\NoteSet;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

/**
 * @property Note $record
 * @return NoteFullFrame
 */
class NoteFullFrame extends \app_RecordView
{    
    /**
     * @var $set NoteSet
     */
    public $set;
    
    protected $noteComponent = null;
    
    public function __construct(\Func_App $App, $id = null, $layout = null)
    {
        $this->set = $App->Note()->recordSet();
        $this->noteComponent = $App->getComponentByName('Note');
        parent::__construct($App, $id, $layout);
    }
    
    /**
     * {@inheritDoc}
     * @see \app_UiObject::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->addInfoSection();
        $this->addSections($this->getView());
        return parent::display($canvas);
    }
    
    protected function getObjectName()
    {
        return $this->App()->classPrefix.'Note';
    }
    
    protected function addInfoSection()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $row = $W->Items()->setSizePolicy('col-md-12');
        
        $row->addItems(
            $noteSection = $W->Section(
                $this->noteComponent->translate('Note'),
                $W->VBoxItems(
                    $this->_summary(),
                    $this->_type()
                )
            )->addClass('box')->setSizePolicy('col-md-12')
        );
        
        $menu = $noteSection->addContextMenu('inline');
        $menu->addClass(\Func_Icons::ICON_LEFT_16);
        $menu->addItem(
            $W->Link(
                '',
                $this->record->getController()->editNoteSection($this->record->id)
            )->addClass('widget-actionbutton', 'section-button', 'icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
        );
        
        $this->addItem($row);
    }
    
    protected function _summary($customSection = null)
    {
        $W = bab_Widgets();
        return $W->Title($this->record->summary, 1);
    }
    
    protected function _type($customSection = null)
    {
        $W = bab_Widgets();
        if($this->record->type()){
            $type = $this->record->type()->name;
            return $W->LabelledWidget(
                $this->noteComponent->translate('Type'),
                $W->Label($type)
            );
        }
        return null;
    }
    
    protected function _description(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        return $W->labelledWidget(
            !empty($label) ? $label : $this->noteComponent->translate('Description'),
            $W->Html($this->record->description),
            $customSection
        );
    }
    
    protected function _attachments(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $attachmentComponent = $App->getComponentByName('ATTACHMENT');
        if(!isset($attachmentComponent)){
            return null;
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $attachmentComponent->translate('Attachments'),
            $App->Controller()->Attachment(false)->_attachmentsList($this->record->getRef()),
            $customSection
        );
    }
}