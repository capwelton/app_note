<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Note\Ui;


/**
 *
 * @param string 	$pathname
 * @param int		$width
 * @param int		$height
 * @return Widget_Image
 */
use Capwelton\App\Note\Set\Note;



class NoteUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * @return NoteTypeTableView
     */
    public function tableView($id = null)
    {
        return $this->NoteTypeTableView($id);
    }
    
    public function NoteTypeTableView($id = null)
    {
        return new NoteTypeTableView($this->app, $id);
    }
    
    /**
     * @return NoteEditor
     */
    public function editor($id = null, \Widget_Layout $layout = null)
    {
        return new NoteEditor($this->app, null, false, $id, $layout);
    }
    
    public function NoteEditor(Note $note, $reloaded = false, $id = null)
    {
        return new NoteEditor($this->app, $note, $reloaded, $id);
    }
    
    
    /**
     * @return NoteTypeEditor
     */
    public function NoteTypeEditor($id = null, \Widget_Layout $layout = null)
    {
        return new NoteTypeEditor($this->app, $id, $layout);
    }
    
    
    public function NotesForAppObject(\app_Record $object, $linkType = null){
        return note_notesForAppObject($object, $linkType);
    }
    
    public function NoteFullFrame(Note $note, $id = null)
    {
        $fullFrame = new NoteFullFrame($this->app, $id);
        $fullFrame->setRecord($note);
        return $fullFrame;
    }
    
    public function NoteSectionEditor(Note $note, $id = null, \Widget_Layout $layout = null)
    {
        return new NoteSectionEditor($note, $this->app, $id, $layout);
    }
    
    public function NoteItem(Note $note)
    {
        return new NoteItem($this->app, $note);
    }
    
    /**
     *
     * @param \Func_App $app
     * @param \app_Record $tags
     * @param \app_Record $associatedObject
     * @param \Widget_Action $tagAction
     * @param int $id
//      * @return \Capwelton\App\Tag\Ui\TagsDisplay
     */
    public function display($app, $tags, \app_Record $associatedObject, \Widget_Action $tagAction = null, $id = null){
        return null;
    }
}

function app_fileAttachementThumbnail($pathname, $width, $height)
{
	$W = bab_Widgets();
    $T = \bab_functionality::get('Thumbnailer');

    $image = $W->Image();

    if ($T) {
         $T->setSourceFile($pathname);
        $T->setBorder(1, '#cccccc', 2, '#ffffff');
        $imageUrl = $T->getThumbnail($width, $height);
        $image->setUrl($imageUrl);
    }

    return $image;
}




/**
 *
 * @param \app_Record $object
 *
 * @return \widget_Frame
 */
function note_notesForAppObject(\app_Record $object, $linkType = null)
{
    $W = bab_Widgets();
    
    $App = $object->App();
    
    $notesFrame = $W->VBoxItems();
    $notesFrame->setVerticalSpacing(0.5, 'em');
    
    $noteSet = $App->NoteSet();
    $controller = $App->Controller()->Note();
    
    $notes = $noteSet->select(
        $noteSet->all(
            $noteSet->isTargetOf($object, $linkType)
        )
    );
    
    $notes->orderDesc($noteSet->pinned);
    $notes->orderDesc($noteSet->modifiedOn);
    
    $nbNotes = 0;
    foreach ($notes as $note) {
        
        if (!$note->isReadable()) {
            continue;
        }
        
        $nbNotes++;
        
        
        $notePreview = $W->Menu()
        ->setLayout($W->FlowLayout())
        ->addClass(\Func_Icons::ICON_LEFT_16);;
        
        if ($note->isReadable()) {
            $notePreview->addItem(
                $W->Link(
                    $App->translate('Display'),
                    $controller->display($note->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }
        if ($note->isUpdatable()) {
            $notePreview->addItem(
                $W->Link(
                    $App->translate('Edit'),
                    $controller->edit($note->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }
        if ($note->isDeletable()) {
            $notePreview->addItem(
                $W->Link(
                    $App->translate('Delete...'),
                    $controller->confirmDelete($note->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }
        $notesFrame->addItem($notePreview);
    }
    
    if ($nbNotes === 0) {
        $notesFrame->addItem(
            $W->Label($App->translate('No note'))
            ->addClass('App-empty')
        );
    }
    return $notesFrame;
}



/**
 *
 * @param Note $note
 *
 * @return \widget_Frame
 */
function app_noteLinkedObjects(Note $note)
{
	$W = bab_Widgets();
    $App = $note->App();

    $App->includeOrganizationSet();

    $objectsFrame = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em');

    $orgs = array();


	$notes = $note->getLinkedNotes();
	if (count($notes) > 0) {
	    foreach ($notes as $parentNote) {
	        $noteFrame = app_noteLinkedObjects($parentNote);
	        $objectsFrame->addItem($noteFrame);
        }
	}

    return $objectsFrame;
}

