<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Note\Ui;
use Capwelton\App\Note\Set\NoteType;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

/**
 * @return NoteTypeEditor
 */
class NoteTypeEditor extends \app_Editor
{
    protected $controller;
    protected $noteType = null;
    protected $noteTypeComponent = null;


    /**
     * @param \Func_App $app
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $app, $id = null, \Widget_Layout $layout = null)
    {
        parent::__construct($app, $id, $layout);
        
        $component = $app->getComponentByName('NoteType');
        if(!$component){
            throw new \app_Exception('The NoteType component has not been found');
        }
        $this->noteTypeComponent = $component;
        
        $this->controller = $app->Controller()->NoteType();
        
        $this->setHiddenValue('tg', $app->controllerTg);
        $this->setSaveAction(
            $this->controller->saveType(),
            $component->translate('Add this note type')
        );
    }


    /**
     * Add fields into form
     */
    public function prependFields($withOptions = true, $withAttachements = true)
    {
        $W = $this->widgets;

        $this->addItem(
            $W->VBoxItems(
                $W->LabelledWidget(
                    $this->noteTypeComponent->translate('Type'),
                    $W->LineEdit()->setMandatory(true, $this->noteTypeComponent->translate('The note type must not be empty')),
                    'name'
                )->addClass('widget-fullwidth'),
                $W->LabelledWidget(
                    $this->noteTypeComponent->translate('Default note type'),
                    $W->CheckBox(),
                    'isDefault'
                )->addClass('widget-fullwidth')
            )
        );
        
        return $this;
    }

    public function setNoteType(NoteType $noteType = null)
    {
        if (isset($noteType)) {
            $this->setRecord($noteType);
            $App = $this->App();
            
            $this->noteType = $noteType;
            $typeValues = $noteType->getValues();
            $this->setValues(array('noteType' => $typeValues));

            if (!empty($noteType->id)) {
                $this->setHiddenValue('data[id]', $noteType->id);
            }
            
            $this->setSaveAction($this->controller->saveType(), $this->noteTypeComponent->translate('Save this note type'));
        }
    }

    /**
     * set editor in create note type mode
     */
    public function addNoteType()
    {
        $this->setNoteType();
    }
}

