<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Note\Ctrl;
use Capwelton\App\Note\Set\Note;
use Capwelton\App\Note\Ui\NoteTypeTableView;
use Capwelton\App\Note\Ui\NoteSectionEditor;
use Capwelton\App\Note\Set\NoteSet;
use Capwelton\App\Note\Set\NoteType;
use Capwelton\App\Note\Ui\NoteTypeEditor;
use Capwelton\App\Note\Ui\NoteUi;
use Capwelton\App\Note\Set\NoteTypeSet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Note');


/**
 * This controller manages actions that can be performed on articles.
 */
class NoteController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('Note'));
    }
    
    /**
     * {@inheritDoc}
     * @see \app_ComponentCtrlRecord::getRecordSet()
     * @param boolean $withDraft Weither or not the defaultCriteria should allow to also return Note records with a draft status
     * @return NoteSet
     */
    protected function getRecordSet($withDraft = false)
    {
        $recordSet = $this->App()->NoteSet();
        if($withDraft){
            $recordSet->setDefaultCriteria($recordSet->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
        }
        return $recordSet;
    }
    
    /**
     * Creates a Note record associated to the $for record with the specified $type (or default if none specified), with a draft status.
     * The note record is automatically saved in the database.
     * @param string $for The \app_Record reference to which the Note will be linked
     * @param int $type The id of the NoteType that the not will have (optional). If no type specified, the default type will be applied
     * @return Note
     */
    private function createDraftRecord($for, $type = null)
    {
        $App = $this->App();
        $set = $this->getRecordSet();
        
        $draftRecord = $set->newRecord();
        $draftRecord->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
        if(isset($type)){
            $noteTypeSet = $App->NoteTypeSet();
            $type = $noteTypeSet->get($noteTypeSet->id->is($type));
            if($type){
                $draftRecord->type = $type->id;
            }
        }
        else{
            $noteTypeSet = $App->NoteTypeSet();
            $defaultType = $noteTypeSet->get($noteTypeSet->isDefault->is(true));
            if($defaultType){
                $draftRecord->type = $defaultType->id;
            }
        }
        
        $draftRecord->save();
        
        $for = $App->getRecordByRef($for, true);
        
        $draftRecord->linkTo($for);
        
        return $draftRecord;
    }
    
    /**
     * {@inheritDoc}
     * @see \app_CtrlRecord::getModifedMessage()
     */
    protected function getModifedMessage()
    {
        $component = $this->App()->getComponentByName('Note');
        return $component->translate('The note has been saved');
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \app_CtrlRecord::getCreatedMessage()
     */
    protected function getCreatedMessage()
    {
        $component = $this->App()->getComponentByName('Note');
        return $component->translate('The note has been saved');
    }
    
    /**
     * {@inheritDoc}
     * @see \app_CtrlRecord::getDeletedMessage()
     */
    protected function getDeletedMessage(\app_Record $record)
    {
        $component = $this->App()->getComponentByName('Note');
        return $component->translate('The note has been deleted');
    }
    
    /**
     * {@inheritDoc}
     * @see \app_CtrlRecord::edit()
     * @return \app_Page
     */
    public function edit($note = null, $reloaded = false, $id = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Note');
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        $page->addClass('app-page-editor');
        
        $noteSet = $this->getRecordSet();
        $note = $noteSet->request($note);
        
        $page->setTitle($appC->translate('Edit note'));
        
        $editor = $Ui->NoteEditor($note, $reloaded, $id);
        
        $editor->setName('data');
        $editor->setRecord($note);
        $editor->setHiddenValue('data[id]',$note->id);
        $editor->isAjax = bab_isAjaxRequest();
        
        $editor->setAjaxAction($this->proxy()->saveChanges(), null, 'change');
        
        $editor->setReloadAction($this->proxy()->edit($note->id, true, $editor->getId()));
        $editor->addClass('depends-' . $this->getRecordClassName());
        $editor->addClass('widget-no-close');
        
        if($reloaded){
            return $editor;
        }
        
        $page->addItem($editor);
        
        if (isset($note)) {
            $page->addContextItem(app_noteLinkedObjects($note));
        }
        
        return $page;
    }
    
    /**
     * Returns a page containing an editor for the record.
     * @param int $noteType The id of the NoteType to edit
     * @throws \app_Exception
     * @return \app_Page
     */
    public function editType($noteType = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $component = $App->getComponentByName('NoteType');
        
        $page = $Ui->Page();
        $page->addClass('app-page-editor');
        
        $recordSet = $App->NoteTypeSet();
        $noteType = $recordSet->get($noteType);
        
        $page->setTitle($component->translate('Edit note type'));
        
        /* @var $editor NoteTypeEditor */
        $editor = $Ui->NoteTypeEditor();
        
        $editor->setName('data');
        $editor->isAjax = bab_isAjaxRequest();
        if($noteType){
            $editor->setNoteType($noteType);
        }
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     * @return \app_Page
     */
    public function comfirmDeleteNoteType($id = null)
    {
        $App = $this->App();
        
        $component = $App->getComponentByName('NoteType');
        
        $W = bab_Widgets();
        $Ui = $App->Ui();
        
        $recordSet = $App->NoteTypeSet();
        $record = $recordSet->get($id);
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($component->translate('Deletion'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($component->translate('The specified note type does not seem to exist'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden()->setName('id'));
        
        $recordTitle = $record->getRecordTitle();
        
        $subTitle = $App->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));
        
        $form->addItem($W->Title($component->translate('Confirm delete?'), 6));
        
        $form->addItem($W->Html(bab_toHtml($component->translate('It will not be possible to undo this deletion'))));
        
        $confirmedAction = $this->proxy()->deleteNoteType($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAjaxAction($confirmedAction)
            ->setLabel($component->translate('Delete'))
        );
        $form->addButton($W->SubmitButton()->setLabel($component->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);
        
        return $page;
    }
    
    /**
     * Deletes the specified record.
     *
     * @requireDeleteMethod
     *
     * @param string $id        The record id
     * @return boolean
     */
    public function deleteNoteType($id)
    {
        $this->requireDeleteMethod();
        
        $App = $this->App();
        
        $component = $App->getComponentByName('NoteType');        
        $recordSet = $App->NoteTypeSet();
        
        $record = $recordSet->request($id);
        
        if (!$record->isDeletable()) {
            throw new \app_AccessException($component->translate('Sorry, You are not allowed to perform this operation'));
        }
        $deletedMessage = $component->translate('The note type has been deleted');
        
        if ($record->delete()) {
            $this->addMessage($deletedMessage);
        }
        
        $this->addReloadSelector('.depends-' . $record->getClassName());
        
        return true;
    }
    
    /**
     * Returns a page containing an editor for the record
     * @param string $for       A \app_Record reference
     * @param int $type         An id of a NoteType record
     * @param int $draft        An id of a Note to edit
     * @return \app_Page|\Widget_VBoxLayout
     */
    public function add($for = null, $type = null, $draft = null, $reloaded = false)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Note');
        $W = bab_Widgets();
        
        /**
         * @var $set \app_TraceableRecordSet
         */
        $set = $this->getRecordSet(true);
        
        if(!$reloaded){
            $draftRecord = $this->createDraftRecord($for, $type);
        }
        else{
            $draftRecord = $set->get($draft);
        }
        
        $infoEditor = $this->getInfoEditor($draftRecord->id);
        $sectionEditor = $this->getSectionEditor($draftRecord->id);
        $sectionEditor->isAjax = true;
        
        $editors = $W->VBoxItems(
            $infoEditor,
            $sectionEditor
        );
        
        if($reloaded){
            return $editors;
        }
        
        $sectionEditor->setSaveAction($this->proxy()->save());
        
        $page = $App->Ui()->Page();
        $page->setTitle($appC->translate('Create a new note'));
        $page->addClass('app-page-editor');
        $page->addClass('widget-no-close');
        
        $page->addItem($editors);
        return $page;
    }
    
    /**
     * Returns an editor for the Note, allowing to modify the title, the private and pinned statuses, and the Note type
     * @param int $note The Note record id
     * @param boolean $ajax Weither the editor will by saved in ajax or not
     * @param string $itemId The editor id
     * @return \app_Editor
     */
    public function getInfoEditor($note, $ajax = true, $itemId = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Note');
        $W = bab_Widgets();
        
        $set = $this->getRecordSet(true);
        $note = $set->request($set->id->is($note));
        
        /* @var $note Note */
        
        $editor = new \app_Editor($App, $itemId);
        $editor->setIconFormat(16, 'left');
        $editor->setName('data');
        $editor->isAjax = true;
        
        $editor->addItem($W->Hidden()->setName('id'));
        
        /**
         * MAIN
         */
        $mainSection = $W->Section(
            $appC->translate('Note'),
            $mainContent = $W->VBoxItems()
        )->addClass('box');
        
        //TYPE
        $noteTypeSet = $App->NoteTypeSet();
        $noteTypes = $noteTypeSet->select();
        
        $select = $W->Select();
        
        foreach ($noteTypes as $noteType) {
            $select->addOption($noteType->id, $noteType->name);
        }
        
        $mainContent->addItem(
            $W->HBoxItems(
                $W->LabelledWidget(
                    $appC->translate('Type'),
                    $select,
                    'type'
                )
            )->addClass('widget-fullwidth')
        );
        
        $mainContent->addItem(
            $W->FlexItems(
                $W->LabelledWidget(
                    $appC->translate('Title'),
                    $W->LineEdit()->setSize(52)->setMaxSize(255)
                    ->setMandatory(true, $appC->translate('The title must not be empty')),
                    'summary'
                )->addClass('note-editor-title'),
                $W->LabelledWidget(
                    $appC->translate('Private'),
                    $W->CheckBox(),
                    'private'
                )->addClass('note-editor-private'),
                $W->LabelledWidget(
                    $appC->translate('Pinned'),
                    $W->CheckBox(),
                    'pinned'
                )->addClass('note-editor-pinned')
            )->setGrowable()->setJustifyContent(\Widget_FlexLayout::FLEX_ALIGN_CONTENT_SPACE_BETWEEN)->setAlignItems(\Widget_FlexLayout::FLEX_ALIGN_CONTENT_FLEX_END)
        );
        
        $editor->addItem($mainSection);
        
        $editor->setRecord($note);
        $editor->setReloadAction($this->proxy()->getInfoEditor($note->id, $ajax, $editor->getId()));
        if($ajax){
            $editor->setAjaxAction($this->proxy()->saveChange(), null, 'change');
        }
        $editor->addClass('depends-' . $this->getRecordClassName());
        $editor->addClass('widget-no-close');
        return $editor;
    }
    
    /**
     * Returns an editor based on the blueprint of the Note type
     * @param int $note The Note record id
     * @param string $itemId The editor id
     * @return NoteSectionEditor
     */
    public function getSectionEditor($note, $itemId = null)
    {
        $App = $this->App();
        $noteSet = $this->getRecordSet(true);
        
        $note = $noteSet->request($noteSet->id->is($note));
        
        /* @var $sectionEditor NoteSectionEditor */
        $sectionEditor = $App->Ui()->NoteSectionEditor($note, $itemId);
        
        $sectionEditor->setName('data');
        $sectionEditor->setRecord($note);
        $sectionEditor->setHiddenValue('data[id]',$note->id);
        $sectionEditor->isAjax = bab_isAjaxRequest();
        
        $sectionEditor->recordSet = $noteSet;
        
        $sectionEditor->addSections($note->getBlueprint());
        
        $sectionEditor->setAjaxAction($this->proxy()->saveChange(), '', 'change input', 5000);
        $sectionEditor->setReloadAction($this->proxy()->getSectionEditor($note->id, $sectionEditor->getId()));
        $sectionEditor->addClass('reload_note_section_editor');
        $sectionEditor->addClass('widget-no-close');
        $sectionEditor->addClass('row');
        
        return $sectionEditor;
    }
    
    /**
     * Saves the Note as a final version (not a draft anymore)
     * {@inheritDoc}
     * @see \app_CtrlRecord::save()
     * 
     * @param array $data
     * @return boolean
     */
    public function save($data = null)
    {
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        return $this->saveAction($data, true);
    }
    
    /**
     * Saves the Note as a draft version
     * {@inheritDoc}
     * @see \app_CtrlRecord::save()
     * 
     * @param array $data
     * @return boolean
     */
    public function saveChange($data = null)
    {
        return $this->saveAction($data, false);
    }
    
    /**
     * Saves the note
     * @param array $data Array containing the Note identification informations and its values
     * @param boolean $finalSave Weither or not the Note should be saved as a final version (not a draft anymore)
     * @return boolean
     */
    public function saveAction($data = null, $finalSave = false)
    {
        $App = $this->App();
        $set = $this->getRecordSet(true);
        
        if (isset($data['id'])) {
            $record = $set->request($data['id']);
        } else {
            $record = $set->newRecord();
        }
        
        /* @var $record Note */
        
        $originalType = $record->type;
        $record->setFormInputValues($data);
        
        if($finalSave){
            $record->deleted = \app_TraceableRecord::DELETED_STATUS_EXISTING;
        }
        
        $record->save();
        
        if (isset($data['for']) && $data['for'] !== '') {
            $references = explode(',', $data['for']);
            foreach ($references as $reference) {
                $subject = $App->getRecordByRef($reference);
                if (!$record->isLinkedTo($subject)) {
                    $record->linkTo($subject);
                }
            }
        }
        
        if($record->type != $originalType){
            $this->addReloadSelector('.reload_note_section_editor');
        }
        
        return true;
    }
    
    /**
     * Display the Note
     * @param int|\app_Record $id 		The note id
     * @return \app_Page|bool
     */
    public function display($id, $view = '', $itemId = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Note');
        $Ui = $App->Ui();
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->get($id);
        if(!isset($record)){
            return true;
        }
        
        if (!$record->isReadable()) {
            throw new \app_AccessException($appC->translate('You do not have access to this page'));
        }
        
        $fullFrame = $Ui->NoteFullFrame($record);
        if(!isset($view) || empty($view)){
            $view = $record->getBlueprint();
        }
        $fullFrame->setView($view);
        $fullFrame->setSizePolicy('widget-min50em');
        
        $page = $Ui->Page();
        $page->setIconFormat(16, 'left');
        if (isset($itemId)) {
            $page->setId($itemId);
        }
        
        if($record->isDeletable())
        {
            $W = bab_Widgets();
            $page->addItem(
                $W->Link(
                    $appC->translate('Delete this note'),
                    $this->proxy()->confirmDelete($id)
                )->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
                ->setSizePolicy('pull-right')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
        }
        
        $page->addItem($fullFrame);
        
        $page->addClass('depends-' . $this->getRecordClassName());
        $page->setReloadAction($this->proxy()->display($id, $view, $page->getId()));
        return $page;
    }
    
    /**
     * Displays an editor of the record values corresponding to the fields of a custom section.
     *
     * @param int $id                   The id of the record to edit
     * @param int $customSectionId      The id of the section used to create the editor
     * @param string $itemId
     *
     * @throws \app_AccessException
     * @return \app_Page
     */
    public function editSection($id, $customSectionId, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Note');
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        
        $customSectionSet = $App->CustomSectionSet();
        $customSection = $customSectionSet->get(
            $customSectionSet->id->is($customSectionId)
        );
        
        $page->setTitle($customSection->name);
        
        $recordSet = $this->getEditRecordSet();
        $record = $recordSet->request($id);
        if (!$record->isUpdatable()) {
            throw new \app_AccessException($appC->translate('You do not have access to this page'));
        }
        
        /* @var $editor NoteSectionEditor */
        $editor = $Ui->NoteSectionEditor($record);
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '_' . __FUNCTION__;
        }
        $page->setId($itemId);
        $editor->isAjax = true;
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->save());
        
        $editor->setAjaxAction($this->proxy()->save(), '', 'save');
        $editor->setName('data');
        $editor->addItem($W->Hidden()->setName('id'));
        
        $editor->recordSet = $recordSet;
        $section = $editor->sectionContent($customSectionId);
        $section->setSizePolicy('widget-60em');
        
        $editor->addItem($section);
        $editor->setRecord($record);
        
        $page->addClass('widget-no-close');
        $page->setReloadAction($this->proxy()->editSection($id, $customSectionId, $page->getId()));
        
        $page->addItem($editor);
        
        return $page;
    }
    
    
    
    /**
     * Displays an editor of the record values corresponding to the fields of a custom section.
     *
     * @param int $id                   The id of the record to edit
     * @param string $itemId
     *
     * @throws \app_AccessException
     * @return \app_Page
     */
    public function editNoteSection($id, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Note');
        
        $page = $App->Ui()->Page();
        
        $page->setTitle($appC->translate('Note'));
        
        $recordSet = $this->getEditRecordSet();
        $record = $recordSet->request($id);
        if (!$record->isUpdatable()) {
            throw new \app_AccessException($appC->translate('You do not have access to this page'));
        }
        
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '_' . __FUNCTION__;
        }
        $editor = $this->getInfoEditor($record->id, false);
        $page->setId($itemId);
        $editor->isAjax = true;
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->save());
        $editor->setName('data');
        $editor->addItem($W->Hidden()->setName('id'));
        
        $editor->recordSet = $recordSet;
        $editor->setRecord($record);
        
        $page->addClass('widget-no-close');
        $page->setReloadAction($this->proxy()->editNoteSection($id, $page->getId()));
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     * Returns a \Widget_VBoxLayout containing minimal informations about the Note (title, files, date, type, actions)
     * @param int $id The Note id
     * @return \Capwelton\App\Note\Ui\NoteItem
     */
    public function listItem($id)
    {
        $App = $this->App();
        
        if ($id instanceof \app_Record) {
            $record = $id;
        } else {
            $record = $this->getRecordSet()->request($id);
        }
        
        return $App->Ui()->NoteItem($record);
    }
    
    /**
     * Return a box containing Note linked to the record given by reference. The Note records are displayed using the listItem method
     * @param string    $for    Record reference
     * @param int       $type   A NoteType id to get only the Note records with this type
     * @param int       $limit  The maximal number of Note to display    
     * @param string    $itemId The 
     * @return \Widget_VBoxLayout
     */
    public function listFor($for, $type = null, $limit = 5, $itemId = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Note');
        $W = bab_Widgets();
        
        $box = $W->VBoxLayout($itemId);
        $box->setReloadAction($this->proxy()->listFor($for, $type, $limit, $box->getId()));
        $box->addClass('depends-' . $this->getRecordClassName());
        
        $forRecord = $App->getRecordByRef($for, true);
        if (!isset($forRecord)) {
            return $box;
        }
        
        $recordSet = $this->getRecordSet();
        
        $conditions = array();
        $conditions[] = $recordSet->isTargetOf($forRecord);
        if(isset($type) && $App->getComponentByName('NoteType')){
            $conditions[] = $recordSet->type->is($type);
        }
        
        $records = $recordSet->select(
            $recordSet->all($conditions)
        );
        $records->orderDesc($recordSet->modifiedOn);
        
        $nbRecords = $records->count();
        $nbItems = 0;
        foreach ($records as $record) {
            if ($nbItems >= $limit) {
                $box->addItem(
                    $W->Link(
                        sprintf($appC->translate('Show all (%d more)'), $nbRecords - $nbItems),
                        $this->proxy()->listFor($for, $type, 100)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-align-center')
                    ->addClass('widget-actionbutton')
                );
                break;
            }
            $nbItems++;
            $listElement = $this->listItem($record);
            $box->addItem(
                $listElement->setSizePolicy('widget-list-element')
            );
        }
        return $box;
    }
    
    /**
     * This methods returns the Record set used to save records
     *
     * @return NoteSet
     */
    protected function getSaveRecordSet()
    {
        return $this->getRecordSet(true);
    }
    
    /**
     * Method to pre save actions on record
     * {@inheritDoc}
     * @see \app_CtrlRecord::preSave()
     * @param Note  $record
     * @param array $data
     */
    protected function preSave(Note $record, $data)
    {
        $record->deleted = \app_TraceableRecord::DELETED_STATUS_EXISTING;
        $record->save();
    }
    
    /**
     * Method to post save actions on record
     * {@inheritDoc}
     * @see \app_CtrlRecord::postSave()
     * @param Note  $record
     * @param array $data
     */
    protected function postSave(Note $record, $data)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        if (isset($data['for'])) {
            $subject = $App->getRecordByRef($data['for']);
            $record->linkTo($subject);
        }
        
        if(isset($data['type'])){
            $record->type = $data['type'];
            $record->save();
        }
        
        if ($iterator = $W->FilePicker()->getTemporaryFiles('attachments')) {
            foreach ($iterator as $file) {
                $record->attachFile($file);
            }
            $W->FilePicker()->cleanup();
        }
    }
    
    
    /**
     * Downloads the selected attached file.
     *
     * @param int		$note		The note id
     * @param string	$filename
     * @return \Widget_Action
     */
    public function downloadAttachment($note = null, $filename = null, $inline = 1)
    {
        $App = $this->App();
        $component = $App->getComponentByName('Note');
        $note = $App->NoteSet()->get($note);
        if (!isset($note)) {
            $this->error($component->translate('The specified note does not seem to exist'));
        }
        $file = bab_Widgets()->FilePicker()->getByName($note->uploadPath(), $filename);
        if (!isset($file)) {
            $this->error($component->translate('The file you requested does not seem to exist'));
        }
        $file->download($inline == 1);
    }
    
    
    /**
     * Does nothing and returns to the previous page.
     *
     * @param array	$message
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
    
    
    /**
     * Returns to the previous page and displays the specified error message.
     *
     * @param string	$errorMessage
     * @return bool
     */
    protected function error($errorMessage)
    {
        $this->addError($errorMessage);
        return true;
    }
    
    
    /**
     * Displays help on this page.
     *
     * @return \Widget_Action
     */
    public function help()
    {
        
    }
    
    /**
     * @param array	$filter
     * @param string $type
     * @return \app_Page
     */
    public function displayTypeList($filter = null, $type = null)
    {
        $page = $this->App()->Ui()->Page();
        
        $itemMenus = $this->getListItemMenus();
        
        foreach ($itemMenus as $itemMenuId => $itemMenu) {
            $page->addItemMenu($itemMenuId, $itemMenu['label'], $itemMenu['action']);
        }
        
        $filteredView = $this->typeFilteredView($filter, $type);
        
        $page->addItem($filteredView);
        
        return $page;
    }
    
    /**
     * @param array     $filter
     * @param string    $type
     * @param string    $itemId
     */
    public function typeFilteredView($filter = null, $type = null, $itemId = null)
    {
        $W = bab_Widgets();
        
        $view = $this->typeModelView($filter, $type, null, $itemId);
        $view->setAjaxAction();
        
        $filter = $view->getFilterValues();
        
        $filterPanel = $view->advancedFilterPanel($filter);
        
        if ($this->getFilterVisibility($itemId)) {
            $filterPanel->addClass('show-filter');
        } else {
            $filterPanel->addClass('hide-filter');
        }
        $toolbar = $this->toolbar($view);
        
        $box = $W->VBoxItems(
            $toolbar,
            $filterPanel
        );
        $box->setReloadAction($this->proxy()->typeFilteredView(null, null, $view->getId()));
        
        return $box;
    }
    
    protected function toolbar($tableView)
    {
        $toolbar = parent::toolbar($tableView);
        
        $W = bab_Widgets();
        $App = $this->App();
        $component = $App->getComponentByName('Note');
        
        $toolbar->addItem(
            $W->FlowItems(
                $W->Link(
                    $component->translate('Edit default blueprint'),
                    $App->Controller()->CustomContainer()->editContainers($App->classPrefix.'Note', $App->NoteTypeSet()->getDefaultBlueprint())
                )->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT),
                $W->Link(
                    $component->translate('Create new note type'),
                    $this->proxy()->editType()
                )->addClass('icon', \Func_Icons::ACTIONS_ARTICLE_NEW)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
            )->setHorizontalSpacing(1, 'em')
        );
        
        return $toolbar;
    }
    
    /**
     * Returns the NoteModelView associated to the NoteSet.
     *
     * @param array|null    $filter
     * @param string        $type
     * @param array|null    $columns    Optional list of columns. array($columnPath] => '1' | '0').
     * @param string|null   $itemId     Widget item id
     * @return NoteTypeTableView
     */
    protected function typeModelView($filter = null, $type = null, $columns = null, $itemId = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $recordSet = $App->NoteTypeSet();
        
        $itemId = $this->getModelViewDefaultId($itemId);
        
        if (!isset($type)) {
            $type = $this->getFilteredViewType($itemId);
        }
        
        /* @var $tableview NoteTypeTableView */
        $tableview = $Ui->NoteTypeTableView($itemId);
        
        $tableview->setRecordController($this);
        
        $tableview->setId($itemId);
        
        $tableview->setRecordSet($recordSet);
        $tableview->addDefaultColumns($recordSet);
        if (isset($filter)) {
            $tableview->setFilterValues($filter);
        }
        $filter = $tableview->getFilterValues();
        
        if (isset($filter['showTotal']) && $filter['showTotal']) {
            $tableview->displaySubTotalRow(true);
        }
        
        $conditions = $tableview->getFilterCriteria($filter);
        
        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );
        
        $records = $recordSet->select($conditions);
        
        $tableview->setDataSource($records);
        
        if (isset($columns)) {
            $availableColumns = $tableview->getVisibleColumns();
            
            $remainingColumns = array();
            foreach ($availableColumns as $availableColumn) {
                $colPath = $availableColumn->getFieldPath();
                if (isset($columns[$colPath]) && $columns[$colPath] == '1') {
                    $remainingColumns[] = $availableColumn;
                }
            }
            $tableview->setColumns($remainingColumns);
        }
        
        $tableview->allowColumnSelection();
        
        return $tableview;
    }    
    
    /**
     * Save a record
     *
     * @requireSaveMethod
     *
     * @param array $data
     */
    public function saveType($data = null)
    {
        $this->requireSaveMethod();
        $App = $this->App();
        
        $component = $App->getComponentByName('NoteType');
        $recordSet = $App->NoteTypeSet();
        
        $pk = $recordSet->getPrimaryKey();
        $message = null;
        $isNewType = false;
        
        if (!empty($data[$pk])) {
            $record = $recordSet->request($data[$pk]);
            unset($data[$pk]);
            
            if (!$record->isUpdatable()) {
                throw new \app_AccessException($component->translate('Access denied'));
            }
            
            $message = $component->translate('The note type has been saved');
        } else {
            
            $record = $recordSet->newRecord();
            
            if (!$recordSet->isCreatable()) {
                throw new \app_AccessException($component->translate('Access denied'));
            }
            
            $message = $component->translate('The note type has been created');
            $isNewType = true;
        }
        
        if (!isset($record)) {
            throw new \app_SaveException($component->translate('The specified note type does not seem to exist'));
        }
        
        
        if (!isset($data)) {
            throw new \app_SaveException($component->translate('Nothing to save'));
        }
        
        $record->setFormInputValues($data);
        
        if ($record->save()) {
            
            if($record->isDefault){
                $allTypes = $recordSet->select($recordSet->id->is($record->id)->_NOT()->_AND_($recordSet->isDefault->is(true)));
                foreach ($allTypes as $type){
                    $type->isDefault = false;
                    $type->save();
                }
            }
            
            if($isNewType){
                $record->setDefaultBlueprint();
            }
            
            if (is_string($message)) {
                $this->addMessage($message);
            }
            
            $this->addReloadSelector('.depends-' . $recordSet->getRecordClassName());
            return true;
        }
        
        return true;
    }
    
    public function getAvailableDisplayFields()
    {
        $availableFields = parent::getAvailableDisplayFields();
        
        unset($availableFields['uuid']);
        unset($availableFields['id']);
        unset($availableFields['createdBy']);
        unset($availableFields['createdOn']);
        unset($availableFields['deleted']);
        unset($availableFields['deletedBy']);
        unset($availableFields['deletedOn']);
        unset($availableFields['modifiedOn']);
        unset($availableFields['modifiedBy']);
        unset($availableFields['summary']);
        unset($availableFields['pinned']);
        unset($availableFields['private']);
        unset($availableFields['type']);
        
        $App = $this->App();
        
        if($attachmentC = $App->getComponentByName('ATTACHMENT')){
            $availableFields['attachments'] = array(
                'name' => 'attachments',
                'description' => $attachmentC->translate('Attached files')
            );
        }
        
        return $availableFields;
    }
}

function app_fileAttachementThumbnail($pathname, $width, $height)
{
    $W = bab_Widgets();
    /* @var $T \Func_Thumbnailer */
    $T = \bab_functionality::get('Thumbnailer');
    
    $image = $W->Image();
    
    if ($T) {
        $T->setSourceFile($pathname);
        $T->setBorder(1, '#cccccc', 2, '#ffffff');
        $imageUrl = $T->getThumbnail($width, $height);
        $image->setUrl($imageUrl);
    }
    
    return $image;
}




/**
 *
 * @param \app_Record $object
 *
 * @return \Widget_VBoxLayout
 */
function note_notesForAppObject(\app_Record $object, $linkType = null)
{
    $W = bab_Widgets();
    
    $App = $object->App();
    $appC = $App->getComponentByName('Note');
    
    $notesFrame = $W->VBoxItems();
    $notesFrame->setVerticalSpacing(0.5, 'em');
    
    $noteSet = $App->NoteSet();
    $controller = $App->Controller()->Note();
    
    $notes = $noteSet->select(
        $noteSet->all(
            $noteSet->isTargetOf($object, $linkType)
        )
    );
    
    $notes->orderDesc($noteSet->pinned);
    $notes->orderDesc($noteSet->modifiedOn);
    
    $nbNotes = 0;
    foreach ($notes as $note) {
        
        if (!$note->isReadable()) {
            continue;
        }
        
        $nbNotes++;
        
        
        $notePreview = $W->Menu()
        ->setLayout($W->FlowLayout())
        ->addClass(\Func_Icons::ICON_LEFT_16);;
        
        if ($note->isReadable()) {
            $notePreview->addItem(
                $W->Link(
                    $appC->translate('Display'),
                    $controller->display($note->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }
        if ($note->isUpdatable()) {
            $notePreview->addItem(
                $W->Link(
                    $appC->translate('Edit'),
                    $controller->edit($note->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }
        if ($note->isDeletable()) {
            $notePreview->addItem(
                $W->Link(
                    $appC->translate('Delete...'),
                    $controller->confirmDelete($note->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }
        $notesFrame->addItem($notePreview);
    }
    
    if ($nbNotes === 0) {
        $notesFrame->addItem(
            $W->Label($appC->translate('No notes'))
            ->addClass('App-empty')
        );
    }
    return $notesFrame;
}



/**
 * @param Note $note
 * @return \Widget_VBoxLayout
 */
function app_noteLinkedObjects(Note $note)
{
    $W = bab_Widgets();
    $App = $note->App();
    
    $App->includeOrganizationSet();
    
    $objectsFrame = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em');
    
    $notes = $note->getLinkedNotes();
    if (count($notes) > 0) {
        foreach ($notes as $parentNote) {
            $noteFrame = app_noteLinkedObjects($parentNote);
            $objectsFrame->addItem($noteFrame);
        }
    }
    
    return $objectsFrame;
}
