<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Note\ComponentDefinition;

/**
 * RecordSets
 */
use Capwelton\App\Note\Set\NoteSet;
use Capwelton\App\Note\Set\NoteTypeSet;

/**
 * Controllers
 */
use Capwelton\App\Note\Ctrl\NoteController;

/**
 * Uis
 */
use Capwelton\App\Note\Ui\NoteUi;

class ComponentDefinition implements \app_ComponentDefinition
{
    public function getDefinition()
    {
        return 'Manages addresses and countries';
    }
    
    public function getComponents(\Func_App $App)
    {
        $noteTypeComponent = $App->createComponent(NoteTypeSet::class, NoteController::class, NoteUi::class);
        $noteComponent = $App->createComponent(NoteSet::class, NoteController::class, NoteUi::class);
        
        return array(
            'NOTETYPE' => $noteTypeComponent,
            'NOTE' => $noteComponent
        );
    }
    
    public function getLangPath(\Func_App $App)
    {
        $addon = $App->getAddon();
        if(!$addon){
            return null;
        }
        return $addon->getPhpPath().'vendor/capwelton/appnote/src/langfiles/';
    }
    
    public function getStylePath(\Func_App $App)
    {
        $addon = $App->getAddon();
        if(!$addon){
            return null;
        }
        return $addon->getPhpPath().'vendor/capwelton/appnote/src/styles/';
    }
    
    public function getScriptPath(\Func_App $App)
    {
        return null;
    }
    
    public function getConfiguration(\Func_App $App)
    {
        $W = bab_Widgets();
        $component = $App->getComponentByName('Note');
        return array(
            array(
                'sectionName' => 'Main',
                'sectionContent' => array(
                    $W->Link(
                        $W->Icon($component->translate('Note types'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->Note()->displayTypeList()
                    )
                )
            )
        );
    }
            
}